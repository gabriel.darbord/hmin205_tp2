package com.example.hmin205_tp3_exo6;

import android.app.Activity;
import android.app.AlertDialog;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;

public class MainActivity extends Activity implements SensorEventListener {
    private float threshold;
    private float oldDist;
    private ImageView iv;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main);

        SensorManager sensorMgr = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor proxi = sensorMgr.getDefaultSensor(Sensor.TYPE_PROXIMITY);

        if (proxi == null) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setMessage("Missing proximity sensor.")
                    .setNegativeButton("Quit", (dialog, id) -> finish())
                    .show();
            return;
        }

        boolean supported = sensorMgr.registerListener(
                this,
                proxi,
                SensorManager.SENSOR_DELAY_UI);

        if (!supported) {
            sensorMgr.unregisterListener(this, proxi);
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setMessage("Accelerometer not supported.")
                    .setNegativeButton("Quit", (dialog, id) -> finish())
                    .show();
            return;
        }

        threshold = proxi.getMaximumRange() / 2;
        iv = findViewById(R.id.image);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onSensorChanged(SensorEvent event) {
        float distance = event.values[0];

        if (distance < threshold && oldDist >= threshold) {
            iv.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_sentiment_very_satisfied_24px));
        } else if (distance >= threshold && oldDist < threshold) {
            iv.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_sentiment_very_dissatisfied_24px));
        } else
            return;

        oldDist = distance;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // rien à faire
    }
}
