package com.example.hmin205_tp3_exo4;

import android.app.Activity;
import android.app.AlertDialog;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends Activity implements SensorEventListener {
    private TextView tv;
    private final float[] directions = new float[2];
    private final float[] deltas = new float[2];

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main);

        SensorManager sensorMgr = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor accel = sensorMgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        if (accel == null) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setMessage("Missing accelerometer.")
                    .setNegativeButton("Quit", (dialog, id) -> finish())
                    .show();
            return;
        }

        boolean supported = sensorMgr.registerListener(
                this,
                accel,
                SensorManager.SENSOR_DELAY_UI);

        if (!supported) {
            sensorMgr.unregisterListener(this, accel);
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setMessage("Accelerometer not supported.")
                    .setNegativeButton("Quit", (dialog, id) -> finish())
                    .show();
            return;
        }

        tv = findViewById(R.id.direction);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        // calcul de l'accélération subie sur les axes X et Y
        deltas[0] = directions[0] - event.values[0];
        directions[0] = event.values[0];
        deltas[1] = directions[1] - event.values[1];
        directions[1] = event.values[1];

        // affichage de la direction proéminente
        if (Math.abs(deltas[0]) > Math.abs(deltas[1])) {
            if (deltas[0] < 0) {
                tv.setText("Gauche");
            } else {
                tv.setText("Droite");
            }
        } else if (deltas[1] < 0) {
            tv.setText("Bas");
        } else {
            tv.setText("Haut");
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // rien à faire
    }
}
