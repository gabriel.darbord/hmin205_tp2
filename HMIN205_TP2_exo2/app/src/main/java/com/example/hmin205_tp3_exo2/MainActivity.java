package com.example.hmin205_tp3_exo2;

import android.app.Activity;
import android.app.AlertDialog;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;

public class MainActivity extends Activity {
    private SensorManager sensorManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        if(sensorManager.getDefaultSensor(Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR) == null) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setMessage("Missing geomagnetic accelerometer, unable to keep track of the phone's orientation.")
                 .setPositiveButton("Continue", null)
                 .setNegativeButton("Quit", (dialog, id) -> finish())
                 .show();
        }
    }
}
