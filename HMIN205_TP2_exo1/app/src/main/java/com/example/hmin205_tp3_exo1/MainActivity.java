package com.example.hmin205_tp3_exo1;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends Activity {
    SensorManager sensorManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main);

        // récupération de la liste des capteurs
        sensorManager = (SensorManager)
                getSystemService(Context.SENSOR_SERVICE);
        listSensor();
    }


    private void listSensor() {
        List<Sensor> sensors = sensorManager.getSensorList(Sensor.TYPE_ALL);
        StringBuilder sensorDesc = new StringBuilder();
        sensorDesc.append(sensors.size()).append(" sensors detected:\n\n");
        for (Sensor sensor : sensors) {
            sensorDesc.append("Name: ").append(sensor.getName()).append("\r\n")
            .append("Type: ").append(sensor.getType()).append("\r\n")
            .append("Version: ").append(sensor.getVersion()).append("\r\n")
            .append("Resolution (in the sensor unit): ").append(sensor.getResolution()).append("\r\n")
            .append("Power in mA used by this sensor while in use: ").append(sensor.getPower()).append("\r\n")
            .append("Vendor: ").append(sensor.getVendor()).append("\r\n")
            .append("Maximum range of the sensor in the sensor's unit: ").append(sensor.getMaximumRange()).append("\r\n")
            .append("Minimum delay allowed between two events in microsecond or zero if this sensor only returns a value when the data it's measuring changes: ").append(sensor.getMinDelay()).append("\r\n")
            .append("\r\n");
        }
        TextView tv = findViewById(R.id.sensor_listing);
        tv.setText(sensorDesc.toString());
    }
}
