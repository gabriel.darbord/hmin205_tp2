package com.example.hmin205_tp3_exo5;

import android.app.Activity;
import android.app.AlertDialog;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;

@RequiresApi(api = Build.VERSION_CODES.M)
public class MainActivity extends Activity implements SensorEventListener {
    private CameraManager cameraMgr;
    private String cameraId;
    private boolean torchState = false;
    private long oldTime;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // gestion de l'accéléromètre
        SensorManager sensorMgr = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor accel = sensorMgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        if (accel == null) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setMessage("Missing accelerometer.")
                    .setNegativeButton("Quit", (dialog, id) -> finish())
                    .show();
            return;
        }

        boolean supported = sensorMgr.registerListener(
                this,
                accel,
                SensorManager.SENSOR_DELAY_UI);

        if (!supported) {
            sensorMgr.unregisterListener(this, accel);
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setMessage("Accelerometer not supported.")
                    .setNegativeButton("Quit", (dialog, id) -> finish())
                    .show();
            return;
        }

        // gestion de la caméra
        cameraMgr = (CameraManager) getSystemService(CAMERA_SERVICE);

        try {
            cameraId = cameraMgr.getCameraIdList()[0];
        } catch (CameraAccessException e) {
            e.printStackTrace();
            finish();
        }
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        float[] values = event.values;
        float x = values[0];
        float y = values[1];
        float z = values[2];

        long curTime = System.currentTimeMillis();
        if(curTime - oldTime < 200)
            return;
        oldTime = curTime;

        float acceleration = (x * x + y * y + z * z)
                / (SensorManager.GRAVITY_EARTH * SensorManager.GRAVITY_EARTH);
        if (acceleration >= 2) {
            torchState = !torchState;
            try {
                cameraMgr.setTorchMode(cameraId, torchState);
            } catch (CameraAccessException e) {
                e.printStackTrace();
                finish();
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // rien à faire
    }
}
