package com.example.hmin205_tp3_exo3;

import android.app.Activity;
import android.app.AlertDialog;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.LinearLayout;

public class MainActivity extends Activity implements SensorEventListener {
    private LinearLayout ll;
    private float inf_threshold, sup_threshold;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main);

        SensorManager sensorMgr = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor accel = sensorMgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        if (accel == null) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setMessage("Missing accelerometer.")
                 .setNegativeButton("Quit", (dialog, id) -> finish())
                 .show();
            return;
        }

        boolean supported = sensorMgr.registerListener(
                this,
                accel,
                SensorManager.SENSOR_DELAY_UI);

        if (!supported) {
            sensorMgr.unregisterListener(this, accel);
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setMessage("Accelerometer not supported.")
                 .setNegativeButton("Quit", (dialog, id) -> finish())
                 .show();
            return;
        }

        // le layout dont la couleur de fond va changer
        ll = findViewById(R.id.mainlayout);

        // séparation des valeurs possibles en tiers
        inf_threshold = accel.getMaximumRange() / 3;
        sup_threshold = inf_threshold * 2;
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        float intensity = 0;

        for(int i=0; i<event.values.length; i++) {
            // accumulation des forces exercées sur l'appareil
            intensity += Math.abs(event.values[i]);
        }

        if(intensity < inf_threshold) {
            ll.setBackgroundColor(getResources().getColor(R.color.green));
        } else if(intensity < sup_threshold) {
            ll.setBackgroundColor(getResources().getColor(R.color.black));
        } else {
            ll.setBackgroundColor(getResources().getColor(R.color.red));
        }
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // rien à faire
    }
}
